
# Build rnaseq-gatk singularity image

## Prerequites :
1. conda and add chanel bioconda
2. singularity
3. GATK 3.8
https://github.com/broadgsa/gatk/releases/tag/3.8-1
`GATK 3.8 jar` file (GenomeAnalysisTK.jar) must be present in current directory.



## Build singularity image :
```
sudo singularity build rnaseq-gatk-singularity rnaseq-gatk-singularity-receipe.txt
```

Use option --use-singularity to run snakemake with this image!

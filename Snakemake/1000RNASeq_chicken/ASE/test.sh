#!/bin/bash
##############################################################################
## launch workflow

# if used on Genologin cluster (INRA Toulouse )
# 	module load system/Python-3.6.3

WORKFLOW_DIR=.

mkdir -p logs

# Full dryrun
snakemake -s $WORKFLOW_DIR/Snakefile --printshellcmds --jobs 200 \
           --configfile config_ase.yaml.example --dryrun > dryrun

snakemake -s $WORKFLOW_DIR/Snakefile --printshellcmds --jobs 200 \
           --configfile config_ase.yaml.example --dag |dot -T png > dryrun.png

# variant filtreation
snakemake -s $WORKFLOW_DIR/Snakefile --printshellcmds --jobs 200 \
           --configfile config_ase.yaml.example --dryrun  Results/vcfFiltration/variants_FsQdBiall.vcf.gz > variantFiltration.dryrun

snakemake -s $WORKFLOW_DIR/Snakefile --printshellcmds --jobs 200 \
           --configfile config_ase.yaml.example --dag Results/vcfFiltration/variants_FsQdBiall.vcf.gz | dot -T png > variantFiltration.png

mkdir -p Results/vcfFiltration Results/Summary
touch Results/vcfFiltration/variants_FsQdBiall.vcf.gz
touch Results/Summary/variants_VariantFiltration_summary.txt
touch Results/vcfFiltration/variants_FsQdBiall_withGTpopCR50_DPgt05rPopCR20_AD.tsv
touch Results/vcfFiltration/variants_FsQdBiall_withGTpopCR50_DPgt05rPopCR20_DP.tsv
touch Results/vcfFiltration/variants_FsQdBiall_withGTpopCR50_DPgt05rPopCR20_GT.tsv

# fastq processing
snakemake -s $WORKFLOW_DIR/Snakefile --printshellcmds --jobs 200 \
           --configfile config_ase.yaml.example --dryrun Results/STAR_Aln_2/samplePE_rg_genomic_pp_rmdup_uniq_split.bam > fastq_process.dryrun

snakemake -s $WORKFLOW_DIR/Snakefile --printshellcmds --jobs 200 \
           --configfile config_ase.yaml.example --dag Results/STAR_Aln_2/samplePE_rg_genomic_pp_rmdup_uniq_split.bam |dot -T png > fastq_process.png

mkdir -p Results/STAR_Aln_2
touch Results/STAR_Aln_2/samplePE_rg_genomic_pp_rmdup_uniq_split.bam
touch Results/STAR_Aln_2/sampleSE_rg_genomic_rmdup_uniq_split.bam
touch Results/STAR_Aln_2/samplePE_rg_genomic_pp_rmdup_uniq_split.bam.bai
touch Results/STAR_Aln_2/sampleSE_rg_genomic_rmdup_uniq_split.bam.bai


# counting process
snakemake -s $WORKFLOW_DIR/Snakefile --printshellcmds --jobs 200 \
           --configfile config_ase.yaml.example --reason --dryrun Results/Counter/samplePE_ASEReadCounter.csv Results/Counter/samplePE_phASER.allelic_counts.txt > counter.dryrun

snakemake -s $WORKFLOW_DIR/Snakefile --printshellcmds --jobs 200 \
           --configfile config_ase.yaml.example --dag Results/Counter/samplePE_ASEReadCounter.csv Results/Counter/samplePE_phASER.allelic_counts.txt |dot -T png > counter.png

# dryrun 2 échantillons : 1 SE et 1 PE : changement dans config_ase.yaml.example
rm -r Results
snakemake -s $WORKFLOW_DIR/Snakefile --printshellcmds --jobs 200 \
           --configfile config_ase.yaml.example --dryrun > 2_samples.dryrun

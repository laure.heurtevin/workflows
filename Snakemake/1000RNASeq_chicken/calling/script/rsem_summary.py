#!/usr/bin/env python3
import re
import os
from collections import OrderedDict

#~ def build_rsem_stats_files(rsem_genes_tpm_summary, rsem_transcripts_tpm_summary, rsem_statistic, nbfiles_rsem_genes,
                          #~ nbfiles_rsem_transcripts, iofiles_list, *io):

def write_rsem_file(out_file, rsem_statistic, res_type, samples, iofiles):
    result_lines = OrderedDict()
    with open(out_file, "w") as rsem_summary_file:
        # Header :
        Header=""
        if res_type == "genes":
            Header="gene_id\ttranscript_id(s)"
        elif res_type == "isoforms":
            Header="transcript_id\tgene_id"
            
        # parse iofiles
        for idx, rs_file in enumerate(iofiles):
            name_sample = samples[idx]
            with open(rs_file, "r") as rsem_file:
                stat_col = None       
                for line in rsem_file:
                    if not stat_col:
                        stat_col = line.strip().split("\t").index(rsem_statistic)
                    else:
                        line=line.strip().split("\t")
                        # ajout du modèle gènes / transcripts au dictionnaire en initialisant les comptages à 0
                        if (line[0], line[1]) not in result_lines:
                            result_lines[(line[0], line[1])] = OrderedDict()
                            for s in samples:
                                if not s in result_lines[(line[0], line[1])]:
                                    result_lines[(line[0], line[1])][s] = 0
                        # accumulation de l'abondance pour le modèle de gène/transcript pour l'échantillon en cours
                        result_lines[(line[0], line[1])][name_sample] += float(line[stat_col])
        
        for h_line, v_line in result_lines.items():
            if len(Header.split("\t"))==2:
                Header += "\t" + "\t".join(result_lines[h_line].keys()) + "\n"
                rsem_summary_file.write(Header)
            rsem_summary_file.write(h_line[0] + "\t" + h_line[1] + "\t" + "\t".join([ str("%.2f" % round(v,2)) for v in v_line.values()]) + "\n")
            
for idx, out_file in enumerate(snakemake.output.out):
    write_rsem_file(out_file, snakemake.params.stat[idx], snakemake.wildcards.feature, snakemake.params.sample_names, list(snakemake.input.count))

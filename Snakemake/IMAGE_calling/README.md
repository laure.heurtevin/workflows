# IMAGE variant calling worklow (Snakemake) documentation

__author__  Kenza BAZI KABBAJ & Maria BERNARD, INRA - SIGENAE

- [What is the IMAGE variant calling worklow?](#what-is-the-image-variant-calling-worklow-)
  - [1. Inputs](#1-inputs)
  - [2. Data preprocessing](#2-data-preprocessing)
  - [3. Variant Calling](#3-variant-calling)
- [Prerequisites](#prerequisites)
- [Configure workflow and running workflow](#configure-workflow-and-running-workflow)



# What is the IMAGE variant calling worklow? 

This pipeline is based on Snakemake. It is able to call variants (SNP/INDEL) with 3 tools (gatk haplotypecaller, freebayes, samtools mpileup).

The pipeline is represented in the graph below:

![](img/IMAGE_calling_workflow.png)

## 1. Inputs

The user must use the [config.yaml](config.yaml) file to provide all necessary inputs for the pipeline:

- reference fasta file indexed with samtools faidx (.fai), picard createDictionnary (.dict) and bwa index
- sample reads description TSV file, with the following columns:
  - ANIMAL ID: Sample Name (not necessarily unique if you have replicate. At the end, you will obtain one gvcf per Sample Name)
  - R1 READS: absolut path of R1 reads
  - R2 READS: absolute path of R2 reads. If single reads, write "None"
  - PLATEFORM: Sequencing Plateform, choices : ILLUMINA, SOLID, LS454, HELICOS and PACBIO.
- a set of known variant in VCF format.
- path to binaries, and the resources.yaml (see [resources_SLURM.yaml](resources_SLURM.yaml)) file to set cluster resources for each programs



The [config.yaml](config.yaml) may also be used to change parameters (mostly calling quality threshold).



## 2. Data preprocessing

- FastQC

This rule allows to make fastqc analysis on fastq reads in order to check on reads quality. 

 

- Mapping 

Aligning fastq reads (PE or SE or both) against reference genome with bwa mem tool. Read groups are added too. 

Remember, that reference genome has to be indexed with bwa index tool in advance.

Mapping parameter (see [config.yaml](config.yaml)):

-k (minimum seed length). By default, this parameter is set to 4.

 

- Sample merging 

Merge bam files for multiple sample replicates (unit) into one using MergeSamFiles from picard tools.

 

- Marking duplicates

Rule for marking PCR and optical duplicates using Picard MarkDuplicates. 

 

- Flagstats

Rule for flagstat analysis after marking duplicates.

 

- Realignment

Rules for indel realignment using GATK RealignerTargetCreator and IndelRealigner tools.

 

- Recalibration 

Rules for base quality recalibration using GATK BaseRecalibrator and PrintReads tools. It will reprocess quality score for each nucleotids in reads based on a set of known variants ( see [config.yaml](config.yaml)). 

Known variants may be extracted from dbSNP database.

In addition, [GATK AnalyzeCovariates](https://software.broadinstitute.org/gatk/documentation/tooldocs/3.8-0/org_broadinstitute_gatk_tools_walkers_bqsr_AnalyzeCovariates.php) tool is used to generate plots in order to visualize the effects of the recalibration process (before and after recalibration). This need to process a second pass of recalibration. 

**The outputs are recalibrated bam files per sample** and PDF statistic report per sample.

 

- Qualimap 

It performs mapping quality on realigned and recalibrated bams using qualimap bamqc tool.

 

## 3. Variant Calling

Note that for each of the 3 callers, by default, we keep only variant with a minimum mapping quality of 30 and minimum base quality of 10( see [config.yaml](config.yaml)). 



- Haplotypecaller 

[GATK HaplotypeCaller](https://software.broadinstitute.org/gatk/documentation/tooldocs/3.8-0/org_broadinstitute_gatk_tools_walkers_haplotypecaller_HaplotypeCaller.php) calls SNPs and INDELs simultaneously via local de-novo assembly of haplotypes in an active region. 

HaplotypeCaller parameter  ( see [config.yaml](config.yaml)). :

-stand_call_conf (Default=30): The minimum phred-scaled confidence threshold at which variants should be called,(and filtered with LowQual if less than the calling threshold)

The calling is done per sample, and finally **individual sample GVCF files** are merge thanks to GATK GenotypeGVCFs into **one multisample VCF file**.

- FreeBayes

[FreeBayes](https://github.com/ekg/freebayes) is a haplotype-based variant detector designed to find small polymorphisms (SNPs, INDELs, MNPs, complex events).

FreeBayes parameters  ( see [config.yaml](config.yaml)):

-pvar (Default=0.0001): Report sites if the probability that there is a polymorphism at the site is greater than N.

-min_supporting_quality (Default=0): Consider any allele in which the sum of qualities of supporting observations is at least Q

-genotype_variant_threshold (Default=0): Limit posterior integration to samples where the second-best genotype likelihood is no more than log(N) from the highest genotype likelihood for the sample.

The calling is done per sample, but all sample VCF files are merge thanks to VCFTools into **one multisample VCF file**. 

- Mpileup

[Samtools mpileup](http://samtools.sourceforge.net/mpileup.shtml) and BCFtools are launched to call variants for detecting SNPs/INDELs. Firstly, mpileup computes and stores the likelihoods of data in BCF format. After that, BCFtools does the SNP/INDEL calling with converting BCF to VCF format.

Mpileup parameter  ( see [config.yaml](config.yaml)):

-C (Default=50): adjust mapping quality

The calling is done per sample, but all sample VCF files are merge thanks to VCFTools into **one multisample VCF file**.

- Bcftools stats 

It uses bcftools stats tool on vcf(s) files to output stats representing variant substitution types, Indel distribution and variant depths.

Statistics are computed for each sample and each caller.



- Multiqc 

Multiqc is used to summarize the outputs from multiple stats (fastqc, metrics (markduplicates), flagstats, qualimap, tables before and after recalibration and bcftools stats output files) into a single HTML report.



# Prerequisites 

snakemake, java8 and R (>=3.4) need to be available in your PATH.

Here is the list of the tools dependacies with the version tested and used for the IMAGE project.

Absolut paths need to be precise in the config.yaml

- bwa v 0.7.17
- picard v 2.18.2
- gatk v 3.7.0 (patch gcfedb67)
- freebayes v 1.1.0
- fastqc v 0.11.2
- samtools v 1.8
- bcftools v 1.6
- qualimap v 11-12-16
- multiqc v 1.5
- vcf concat v 0.1.15
- vcf merge v 0.1.15
- bgzip v 0.2.5
- tabix v 0.2.5
- md5sum v 8.22

# Configure workflow and running workflow

- Copy the config.yaml file into your working directory and update it according to the instructions inside the file.
- If necessary update resources_SLURM.yaml to define cpu, memory and cluster partition resources for each rule. The default section is the minimal resources per job. The other section take as name the rules name and define only resources that are different from the default section.
- launch snakemake command as presented in SLURM.sh to launch the workflow.

Resources_SLURM.yaml and SLURM.sh are specific to SLURM cluster. Therefore, if you use another cluster, you need to create other files. However, you can use these files to inspire you.
​    

This is the official documentation of Snakemake if you need more information: <https://snakemake.readthedocs.io/en/stable/tutorial/tutorial.html>

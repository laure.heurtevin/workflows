# coding: utf-8

__author__ = "Kenza BAZI KABBAJ / Maria BERNARD- Sigenae"
__version__ = "1.0.0"



"""
Rule for variant calling with GATK haplotypeCaller tool. The outputs are zipped g.vcf file(s) per sample.
"""


rule HaplotypeCaller:
    input:
        bam="results/gatk_recalibration/{sample}.rg.sort.md.real.recal.bam",
        ref=config["reference_genome"]
        
    output:
        gvcf="results/gatk_haplotypecaller/{sample}.rg.sort.md.real.recal.g.vcf.gz",
        index="results/gatk_haplotypecaller/{sample}.rg.sort.md.real.recal.g.vcf.gz.tbi"
    
    log: 
        stderr="results/gatk_haplotypecaller/logs/{sample}_calling.stderr",
        stdout="results/gatk_haplotypecaller/logs/{sample}_calling.stdout"

    threads: config["HaplotypeCaller"]["cpu"]

    params:
        mem=config["HaplotypeCaller"]["mem"],
        mmq="30" if config["min_mapping_quality"]=="" else config["min_mapping_quality"],
        mbq="10" if config["min_base_quality"]=="" else config["min_base_quality"]
        
    shell:
        """
        java -Xmx{params.mem} -jar {config[bin][gatk]} -T HaplotypeCaller -nct {threads} -R {input.ref} -I {input.bam} -o {output.gvcf} -ERC GVCF -variant_index_type LINEAR -variant_index_parameter 128000 -mmq {params.mmq} -mbq {params.mbq} 2> {log.stderr} > {log.stdout}
        """

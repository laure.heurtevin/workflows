#!/bin/bash
##############################################################################
## launch workflow

# if used on Genologin cluster (INRA Toulouse )
# 	module load system/R-3.4.3
# 	module load system/Python-3.6.3
# and because we are not using module for all other dependencies, you need to add vcftool librairy in the PERL5LIB environment variable
#   export PERL5LIB=/usr/local/bioinfo/src/VCFtools/vcftools-0.1.15/src/perl/:$PERL5LIB

WORKFLOW_DIR=??

mkdir -p logs

# This is an example of the snakemake command line to launch a dryrun
snakemake -s $WORKFLOW_DIR/Snakefile --printshellcmds --jobs 200 \
           --configfile config.yaml --dryrun

# This is an example of the snakemake command line to launch on a SLURM cluster
	# {cluster.cpu}, {cluster.mem} and {cluster.partition} will be replace by value defined in the resources_SLURM.yaml file
snakemake -s $WORKFLOW_DIR/Snakefile --printshellcmds --jobs 200 \
           --configfile config.yaml \
           --cluster-config $WORKFLOW_DIR/resources_SLURM.yaml \
           --cluster "sbatch -p {cluster.partition} --cpus-per-task={cluster.cpu} --mem-per-cpu={cluster.mem} --error=logs/%x.stderr --output=logs/%x.stdout " --latency-wait 30

# This is an example of the snakemake command line to launch on a SLURM cluster
	# {cluster.cpu}, {cluster.mem} and {cluster.partition} will be replace by value defined in the resources_SLURM_memGlobal.yaml file
# The difference is that we defined global memory instead of memory per cpu
#~ snakemake -s $WORKFLOW_DIR/Snakefile --printshellcmds --jobs 200 \
           #~ --configfile config.yaml \
           #~ --cluster-config $WORKFLOW_DIR/resources_SLURM.yaml \
           #~ --cluster "sbatch -p {cluster.partition} --cpus-per-task={cluster.cpu} --mem={cluster.mem} --error=logs/%x.stderr --output=logs/%x.stdout " --latency-wait 30

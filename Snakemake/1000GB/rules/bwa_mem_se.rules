# coding: utf-8

__author__ = "Kenza BAZI KABBAJ - Sigenae"
__version__ = "1.0.0"

from snakemake.exceptions import MissingInputException


"""
Rules for mapping trimmed single-end reads with bwa mem. Bam file(s) are sorted and indexed with samtools.
"""

# Get ID names of each SE sample
UNIT_TO_SAMPLE_SE = {
    ID: sample for sample, units in SE_samples.items()
    for ID in units}


rule bwa_mem_se:
    input:
        config["reference_genome"],
        "results/trimmomatic/SE/{unit}.fastq.gz"
        
    output:
        bam=temp("results/mapping/SE/{unit}-se.bam")
    
    params:
        ID=lambda wildcards: UNIT_TO_SAMPLE_SE[wildcards.unit],
        PLATEFORM=lambda wildcards: plateform[UNIT_TO_SAMPLE_SE[wildcards.unit]]
    
    threads: config["bwa_mem_se"]["cpu"]

    log:
        "results/mapping/SE/logs/{unit}_mapping_SE.stderr",
        "results/mapping/SE/logs/{unit}_samtools_view_SE.stderr"

    shell:
        "{config[bwa]} mem -M -t {threads} -R '@RG\\tID:{wildcards.unit}\\tPL:{params.PLATEFORM}\\tSM:{params.ID}' {input} 2> {log[0]} | {config[samtools]} view -@ {threads} -bS - > {output.bam} 2> {log[1]}"


rule bwa_se_sort:
    input:
        temp("results/mapping/SE/{unit}-se.bam")
    output:
        temp("results/mapping/SE/{unit}-se.sorted.bam")
    threads:
        config["bwa_se_sort"]["cpu"]
    log:
        "results/mapping/SE/logs/{unit}_samtools_sort_SE.stderr"
    shell:
        "{config[samtools]} sort {input} -@ {threads} -o {output} 2> {log}"

rule bwa_se_index:
    input:
        temp("results/mapping/SE/{unit}-se.sorted.bam")
    output:
        temp("results/mapping/SE/{unit}-se.sorted.bam.bai")
    threads:
        config["bwa_se_index"]["cpu"]
    log:
        "results/mapping/SE/logs/{unit}_samtools_index_SE.stderr"
    shell:
        " {config[samtools]} index {input} -@ {threads} {output} 2> {log}"

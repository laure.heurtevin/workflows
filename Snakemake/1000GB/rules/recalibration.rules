# coding: utf-8

__author__ = "Kenza BAZI KABBAJ - Sigenae"
__version__ = "1.0.0"

"""
Rules for base quality recalibration using GATK BaseRecalibrator and PrintReads tools. It will reprocess quality score for each nucleotids in reads based on a set of known variants. In addition, AnalyzeCovariates tool is used to generates plots in order to visualize the effects of the recalibration process (before and after recalibration). The outputs are recalibrated bam files per sample and pdf statistic report per sample.
"""

rule baserecalibrator_1:
    input:
        bai="results/marked_bams/{sample}_dedup.bai",
        bam="results/marked_bams/{sample}_dedup.bam",
        ref=config["reference_genome"],
        known_variants=config["known_variants"]
    output:
        protected("results/recalibrated_bams/{sample}.before.recal.table")
    threads: config["baserecalibrator_1"]["cpu"]
    log: 
        stderr="results/recalibrated_bams/logs/{sample}_baserecalibrator_1.stderr",
        stdout="results/recalibrated_bams/logs/{sample}_baserecalibrator_1.stdout"
    params:
        mem= config["baserecalibrator_1"]["mem"]
    shell:
        "java -Xmx{params.mem} -jar {config[gatk]} -T BaseRecalibrator -nct {threads} -R {input.ref} -I {input.bam} -knownSites:vcf {input.known_variants} -o {output} 2> {log.stderr} > {log.stdout}"

rule printreads:
    input:
        bai="results/marked_bams/{sample}_dedup.bai",
        bam="results/marked_bams/{sample}_dedup.bam",
        recal_table="results/recalibrated_bams/{sample}.before.recal.table",
        ref=config["reference_genome"]
    output:
        protected("results/recalibrated_bams/{sample}_dedup_recal.bam")
    threads: config["printreads"]["cpu"]
    log: 
        stderr="results/recalibrated_bams/logs/{sample}_printreads.stderr",
        stdout="results/recalibrated_bams/logs/{sample}_printreads.stdout"
    params:
        mem=config["printreads"]["mem"]
    shell:
        "java -Xmx{params.mem} -jar {config[gatk]} -T PrintReads -nct {threads} -R {input.ref} -I {input.bam} -BQSR {input.recal_table} -o {output} 2> {log.stderr} > {log.stdout}"

rule baserecalibrator_2:
    input:
        bai="results/marked_bams/{sample}_dedup.bai",
        bam="results/marked_bams/{sample}_dedup.bam",
        ref=config["reference_genome"],
        known_variants=config["known_variants"],
        recal_table="results/recalibrated_bams/{sample}.before.recal.table"
    output:
        protected("results/recalibrated_bams/{sample}.after.recal.table")
    threads: config["baserecalibrator_2"]["cpu"]
    log:
        stderr="results/recalibrated_bams/logs/{sample}_baserecalibrator_2.stderr",
        stdout="results/recalibrated_bams/logs/{sample}_baserecalibrator_2.stdout"
    params:
        mem= config["baserecalibrator_2"]["mem"]
    shell:
        "java -Xmx{params.mem} -jar {config[gatk]} -T BaseRecalibrator -nct {threads} -R {input.ref} -I {input.bam} -knownSites:vcf {input.known_variants} -BQSR {input.recal_table} -o {output} 2> {log.stderr} > {log.stdout}"

rule analyzeCovariates:
    input:
        before_recal_table="results/recalibrated_bams/{sample}.before.recal.table",
        after_recal_table="results/recalibrated_bams/{sample}.after.recal.table",
        ref=config["reference_genome"]
    output:
        protected("results/recalibrated_bams/{sample}_recal_plots.pdf")
    log:
        stderr="results/recalibrated_bams/logs/{sample}_analyzeCovariates.stderr",
        stdout="results/recalibrated_bams/logs/{sample}_analyzeCovariates.stdout"
    params:
        mem= config["analyzeCovariates"]["mem"]
    shell: "java -Xmx{params.mem} -jar {config[gatk]} -T AnalyzeCovariates -R {input.ref} -before {input.before_recal_table} -after {input.after_recal_table} -plots {output} 2> {log.stderr} > {log.stdout}"


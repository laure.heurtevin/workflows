# coding: utf-8

__author__ = "Kenza BAZI KABBAJ - Sigenae"
__version__ = "1.0.0"

"""
Rules for BamStats. This step will collect statistics and generate a bed file detailing callable, uncallable, poorly mapped and other parts of the genome using GATK CallableLoci. Secondly, the average read coverage is calculated using Gatk DepthOfCoverage.
"""

rule callableLoci:
    input:
        bam="results/recalibrated_bams/{sample}_dedup_recal.bam",
        ref=config["reference_genome"]
    output:
        summary=protected("results/BamStats/{sample}.CallableLoci.summary.txt"),
        CallableLoci=protected("results/BamStats/{sample}.CallableLoci.bed")
    log:
        stderr="results/BamStats/logs/{sample}_CallableLoci.stderr",
        stdout="results/BamStats/logs/{sample}_CallableLoci.stdout"
    params:
        mem=str(config["callableLoci"]["mem"])
    shell:
        "java -Xmx{params.mem} -jar {config[gatk]} -T CallableLoci -R {input.ref} -I {input.bam} -summary {output.summary} -o {output.CallableLoci} 2> {log.stderr} > {log.stdout}"

rule depthOfCoverage:
    input:
        bam="results/recalibrated_bams/{sample}_dedup_recal.bam",
        ref=config["reference_genome"]
    output:
        protected("results/BamStats/{sample}_dedup_recal.coverage.sample_cumulative_coverage_counts"),
        protected("results/BamStats/{sample}_dedup_recal.coverage.sample_interval_statistics"),
        protected("results/BamStats/{sample}_dedup_recal.coverage.sample_interval_summary"),
        protected("results/BamStats/{sample}_dedup_recal.coverage.sample_statistics"),
        protected("results/BamStats/{sample}_dedup_recal.coverage.sample_summary"),
        protected("results/BamStats/{sample}_dedup_recal.coverage.sample_cumulative_coverage_proportions")
    log:
        stderr="results/BamStats/logs/{sample}_DepthOfCoverage.stderr",
        stdout="results/BamStats/logs/{sample}_DepthOfCoverage.stdout"
    params:
        mem=str(config["depthOfCoverage"]["mem"])
    shell:
        "java -Xmx{params.mem} -jar {config[gatk]} -T DepthOfCoverage -R {input.ref} -I {input.bam} --omitDepthOutputAtEachBase --logging_level ERROR --summaryCoverageThreshold 10 --summaryCoverageThreshold 20 --summaryCoverageThreshold 30 --summaryCoverageThreshold 40 --summaryCoverageThreshold 50 --summaryCoverageThreshold 80 --summaryCoverageThreshold 90 --summaryCoverageThreshold 100 --summaryCoverageThreshold 150 --minBaseQuality 15 --minMappingQuality 30 --start 1 --stop 1000 --nBins 999 -dt NONE -o results/BamStats/{wildcards.sample}_dedup_recal.coverage 2> {log.stderr} > {log.stdout}"


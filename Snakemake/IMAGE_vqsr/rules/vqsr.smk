# coding: utf-8

__author__ = "Kenza BAZI KABBAJ / Maria BERNARD- Sigenae"
__version__ = "1.0.0"

"""
Rules to applied Variant Quality Score Recalibration
"""
wildcard_constraints:
    var = "SNP|INDEL"

def get_trainingSet(wildcards):
    
    input_list=list()
    for prefix in trainingSets:
        if prefix == 'VQSR_trainingSet_trusted_3callers_hardFiltered_variants' : 
            input_list.append("results/hard_filtering/VQSR_trainingSet_trusted_3callers_hardFiltered_variants_" + wildcards.var + ".vcf.gz")
        else:
            input_list.append("results/split_SNP_INDEL/" + prefix + "_" + wildcards.var + ".vcf.gz")

    return input_list

rule VariantRecalibrator :
    input :
        ref = config["reference_genome"],
        vcf = "results/split_SNP_INDEL/{GATK_input_prefix}_{var}.vcf.gz",
        resources = get_trainingSet
    output :
        recalFile = temp("results/VQSR/{GATK_input_prefix}_{var}.recal"),
        tranchesFile = temp("results/VQSR/{GATK_input_prefix}_{var}.tranches"),
        R = temp("results/VQSR/{GATK_input_prefix}_{var}.R")
    log :
        "results/VQSR/logs/{GATK_input_prefix}_{var}.varRecal.log"
    params:
        trainings = trainingSets.values(),
        mem = config["VariantRecalibrator"]["mem"]
    message:
        "\nrule VariantRecalibrator:\n" \
        + "\tinput: {input}\n" \
        + "\toutput: {output}\n" \
        + "\tlog: {log}\n" \
        + "Executing VariantRecalibrator with the training sets you provide, commonly called filtered variant and uniquely called variant\nSee {log} for the detailed command line\n" \
        + "java -Xmx{params.mem} -jar {config[bin][gatk]} -T VariantRecalibrator -R {input.ref} -input {input.vcf} TRAININGSET -an QD -an DP -an FS -an MQRankSum -an ReadPosRankSum -an SOR -an MQ -mode {wildcards.var} -recalFile {output.recalFile} -tranchesFile {output.tranchesFile} -rscriptFile {output.R} 2> {log}"
    run:
        models = " ".join([x for t in zip(params.trainings, input.resources) for x in t])
        shell("java -Xmx{params.mem} -jar {config[bin][gatk]} -T VariantRecalibrator -R {input.ref} -input {input.vcf} " + models + " -an QD -an DP -an FS -an MQRankSum -an ReadPosRankSum -an SOR -an MQ -mode {wildcards.var} -recalFile {output.recalFile} -tranchesFile {output.tranchesFile} -rscriptFile {output.R} 2> {log}")



rule ApplyRecalibration:
    input:
        ref = config["reference_genome"],
        vcf = "results/split_SNP_INDEL/{GATK_input_prefix}_{var}.vcf.gz",
        recalFile = "results/VQSR/{GATK_input_prefix}_{var}.recal",
        tranchesFile = "results/VQSR/{GATK_input_prefix}_{var}.tranches",
        R = "results/VQSR/{GATK_input_prefix}_{var}.R"
    output:
        vcf = "results/VQSR/{GATK_input_prefix}_vqsr_{var}.vcf.gz",
        idx = temp("results/VQSR/{GATK_input_prefix}_{var}.recal.idx")
    log:
        "results/VQSR/logs/{GATK_input_prefix}_vqsr_{var}.log"
    params:
        mem = config["ApplyRecalibration"]["mem"],
        filter_level = config["filter_level"]
    shell:
        "java -Xmx{params.mem} -jar {config[bin][gatk]} -T ApplyRecalibration -R {input.ref} -input {input.vcf} -mode {wildcards.var} --ts_filter_level {params.filter_level} -recalFile {input.recalFile} -tranchesFile {input.tranchesFile} -o {output.vcf} 2> {log}"

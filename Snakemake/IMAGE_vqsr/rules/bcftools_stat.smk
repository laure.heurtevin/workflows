# coding: utf-8

__author__ = "Maria BERNARD - Sigenae"
__version__ = "1.0.0"

"""
Compute statistics on final vcf file
"""
#sed -i 's/{wildcards.sample}.rg.sort.md.real.recal.g.vcf.gz/{wildcards.sample}_gatk/g' {output} 


rule bcftools_stats:
    input:
        "results/genoFilter/{gatk_prefix}_vqsr_{var}_genFiltered.vcf.gz"
    output:
        "results/bcftools_stats/{gatk_prefix}_vqsr_{var}_genFiltered.stats"
    log:
        "results/bcftools_stats/logs/{gatk_prefix}_vqsr_{var}_genFiltered.stats.log"
    threads:
        config["bcftools_stats"]["cpu"]
    shell:
        """
        {config[bin][bcftools]} stats {input} --threads {threads} > {output} 2> {log}        
        """

# coding: utf-8

__author__ = "Kenza BAZI KABBAJ / Maria BERNARD- Sigenae"
__version__ = "1.0.0"

"""
Rule to extract uniquely called variant on the one hand and the commonly called variant on the other (GATK format is kept)
"""

def get_ordered_input_uniq(wildcards):
    input = []
    if wildcards.caller == "freebayes" :
        input = [config["Freebayes_variants"], config["Mpileup_variants"], config["GATK_variants"] ]
    elif wildcards.caller == "mpileup" :
        input = [config["Mpileup_variants"], config["GATK_variants"], config["Freebayes_variants"] ]
    elif wildcards.caller == "gatk" :
        input = [config["GATK_variants"], config["Freebayes_variants"], config["Mpileup_variants"] ]
    
    return input

rule uniq_variants :
    input :  
        get_ordered_input_uniq
    output :
        vcf = temp("results/intersect/uniq_{caller}_variants.vcf"),
        gzip = temp("results/intersect/uniq_{caller}_variants.vcf.gz")
    log:
        "results/intersect/logs/uniq_{caller}_variants.log"
    shell:
        """
        {config[bin][bcftools]} isec -w 1 -c all -C -O v {input} | cut -f 1-8 > {output.vcf} 2> {log}
        {config[bin][bgzip]} -c {output.vcf} > {output.gzip}
        """

rule concat_uniq_variants:
    input : 
        expand("results/intersect/uniq_{caller}_variants.vcf.gz.tbi", caller=["freebayes","mpileup","gatk"]),
        vcfs=expand("results/intersect/uniq_{caller}_variants.vcf.gz", caller=["freebayes","mpileup","gatk"]),
    output :
        vcf = temp("results/intersect/VQSR_trainingSet_untrusted_uniquely_called_variants.vcf.gz")
    log :
        "results/intersect/logs/concat_uniq_variants.log"
    params:
        mem=config["concat_uniq"]["mem"]
    shell:
        """
        {config[bin][bcftools]} concat -a {input.vcfs} | {config[bin][bcftools]} sort - -m {params.mem} -o {output} -O z
        """

rule common_variants:
    input:
        config["GATK_variants"], 
        config["Freebayes_variants"], 
        config["Mpileup_variants"]
    output:
        vcf = temp("results/intersect/common_variants.vcf"),
        gzip = temp("results/intersect/common_variants.vcf.gz")
    log:
        "results/intersect/logs/common_variants.log"
    shell:
        """
        {config[bin][bcftools]} isec -c all -w 1 -n =3 -O v {input} > {output.vcf} 2> {log}
        {config[bin][bgzip]} -c {output.vcf} > {output.gzip}
        """

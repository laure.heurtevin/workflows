# coding: utf-8

__author__ = "Kenza BAZI KABBAJ / Maria BERNARD- Sigenae"
__version__ = "1.0.0"

"""
Rule to split vcf in SNP.vcf and INDEL.vcf
"""
wildcard_constraints:
    var = "SNP|INDEL"
    
def get_input_vcf(wildcards) :
    if wildcards.prefix == "common" :
        return "results/intersect/common_variants.vcf.gz"
    elif wildcards.prefix == "VQSR_trainingSet_untrusted_uniquely_called_variants":
        return "results/intersect/VQSR_trainingSet_untrusted_uniquely_called_variants.vcf.gz"
    elif wildcards.prefix == gatk_prefix:
        return config['GATK_variants']
    else:
        for Set in config['vqsr_resources']:
            if os.path.basename(Set['file']).startswith(wildcards.prefix):
                return Set['file']
                                
def get_input_index(wildcards) :
    if wildcards.prefix == "common" :
        return "results/intersect/common_variants.vcf.gz.tbi"
    elif wildcards.prefix == "VQSR_trainingSet_untrusted_uniquely_called_variants":
        return "results/intersect/VQSR_trainingSet_untrusted_uniquely_called_variants.vcf.gz.tbi"
    elif wildcards.prefix == gatk_prefix:
        return config['GATK_variants'] + ".tbi"
    else:
        for Set in config['vqsr_resources']:
            if os.path.basename(Set['file']).startswith(wildcards.prefix):
                return Set['file'] + ".tbi"

rule split_vcf:
    input :
        vcf = get_input_vcf ,
        tbi = get_input_index ,
        ref = config["reference_genome"]
    output :
        vcf = "results/split_SNP_INDEL/{prefix}_{var}.vcf.gz"
    log :
        "results/split_SNP_INDEL/logs/{prefix}_{var}.log"
    params:
        mem= config["split_vcf"]["mem"]
    shell:
        "java -Xmx{params.mem} -jar {config[bin][gatk]} -T SelectVariants -R {input.ref} -V {input.vcf} --excludeFiltered -selectType {wildcards.var} -o {output.vcf} 2> {log}"
        

 # IMAGE VQSR Filter worklow (Snakemake) documentation

__author__  Kenza BAZI KABBAJ & Maria BERNARD, INRA - SIGENAE

- [What is VQSR Filter ?](#what-is-vqsr-filter--)
  - [1. Inputs](#1-inputs)
  - [2. Data Processing](#2-data-processing)
    - [Intersection](#intersection)
    - [Separate SNP/INDEL calls](#separate-snp-indel-calls)
    - [Commonly called variants hard filtering](#commonly-called-variants-hard-filtering)
    - [Variant Quality Score Recalibration](#variant-quality-score-recalibration)
    - [Genotype Filtering](#genotype-filtering)
- [3. Dependencies](#3-dependencies)
- [4. Configure and running workflow](#4-configure-and-running-workflow)



## What is VQSR Filter ?

**VQSR Filter** is a pipeline based on Snakemake. It will filter gatk variants (vcf) by Variant Quality Score Recalibration (VQSR) from [GATK tools](<https://software.broadinstitute.org/gatk/documentation/article.php?id=39>) and remove ambiguous genotypes, in order to generate a highly accurate call sets.

 The pipeline is represented in the graph below:

![](img/IMAGE_vqsr_workflow.png)



VQSR uses machine learning algorithms to learn from known dataset what is the annotation profile of good variants vs. bad variants. It will finally be able to compute a calibrate score which allow finally to better filtered out poor quality variants. To do that, VQSR need external training datasets of good and bad quality. In our case we will use at least :

- As a positive training dataset : variants called by the three callers (see [IMAGE calling workflow](<https://forgemia.inra.fr/bios4biol/workflows/tree/master/Snakemake/IMAGE_calling>), and filtered on individuals metrics following the [GATK recommandations]([https://software.broadinstitute.org/gatk/documentation/article.php?id=3225](https://emea01.safelinks.protection.outlook.com/?url=https%3A%2F%2Fsoftware.broadinstitute.org%2Fgatk%2Fdocumentation%2Farticle.php%3Fid%3D3225&data=02%7C01%7Clicia.colli%40unicatt.it%7C60043b2a691540cfffe208d697d2378c%7Cb94f7d7481ff44a9b5886682acc85779%7C0%7C0%7C636863327455945360&sdata=nw0k5dY0AVUvv7smOFi9tz5ymMW66C5B2m62VnaZx0c%3D&reserved=0) )

- As a negative training dataset : variants unfiltered called by only one caller.

 

The pipeline will also use as annotation database, a more general external dataset (like dbSNP) to stratify output metrics such as Ti/Tv ratio.



## 1. Inputs

 

The user must use the [config.yaml](config.yaml) file to provide all necessary inputs for the pipeline:

- A reference fasta file indexed with samtools faidx (.fai), and picard createDictionnary (.dict)
- A tab delimited file with average depth per sample.
- 3 VCF files (compressed and indexed with bgzip and tabix) corresponding to the variants called with GATK, [Freebayes](<https://github.com/ekg/freebayes>) and [Samtools Mpileup](<http://samtools.sourceforge.net/mpileup.shtml>)
- At least one VCF file used as training set, such as the dbSNP VCF file. For more information, see <https://gatkforums.broadinstitute.org/gatk/discussion/1259/which-training-sets-arguments-should-i-use-for-running-vqsr>

 

## 2. Data Processing

### Intersection

 In order to use VQSR, it’s important to have variant resources following GATK specifications. The most important resource to provide is a highly accurate set of variants. Please see GATK documentation: <https://gatkforums.broadinstitute.org/gatk/discussion/1259/which-training-sets-arguments-should-i-use-for-running-vqsr>

 You can provide these resources with training-set options [config.yaml](config.yaml) if available, but in any case the pipeline will generate 2 resources from the callings provided as inputs, using [ bcftools isec](<https://samtools.github.io/bcftools/bcftools.html#isec>) tool :

- Commonly_called_variants.vcf: variants in common between the 3 callers (Gatk, Freebayes, Samtools Mpileup), which will be filtered later for better accuracy.
- Uniquely_called_variants.vcf: variants detected by only one of the three callers

 

### Separate SNP/INDEL calls

 

Each VCF file (Commonly_called, Uniquely_called, and all training_set provided as input) is split in 2 separates files, one for the SNP variant and one for the INDEL variants using GATK SelectVariants

### Commonly called variants hard filtering

Commonly called variants are filtered by VariantFiltration following [Gatk recommandations](<https://software.broadinstitute.org/gatk/documentation/article.php?id=3225>) in order to have a more accurate call set:

- For SNPs: 

  ` QD < 2.0; MQ < 40.0; FS > 60.0; SOR > 3.0; MQRankSum < -12.5; ReadPosRankSum < -8.0`

- For INDEls: 

  ` QD < 2.0; ReadPosRankSum < -20.0; InbreedingCoeff < -0.8; FS > 200.0; SOR > 10.0`



### Variant Quality Score Recalibration

Variant Quality Score Recalibration (VQSR) from GATK is designed to filter gatk vcf results by building a variant recalibration model with VariantRecalibrator tool using training set(s) and applying the desired level of recalibration to the variants with ApplyRecalibration tool. It aims to refine the call set by reducing the amount of false positives. Note that VQSR is done separately on SNPs or INDELs.

Each training set is defined with 4 characteristics (see [config.yaml](config.yaml)). For the 2 set generating automatically, the following parameters is used:

- For common variants: 

  `name=common_variants, truth=True, training=True, known=false, prior=15.0`

  It means that these variants are true positives, *truth=True* and will be used to train the model, *training=True*. The most important parameter is *prior* which determines how much confident we are about these variants. In this case, 15.0 means that 96.84% are true variants.

- For unique variants: 

  `name=unique_variants, truth=False, training=True, known=false, prior=10.0`.

  These variants are representing non true sites, *truth=False*, which contains too many false positives.



After recalibrating GATK variants, we need to specify the desired level of truth sensitivity (ts-filter-level) to ApplyRecalibration. This step allows us to choose how much sensitive or specific we want to be. Following [GATK recommendations](https://gatkforums.broadinstitute.org/gatk/discussion/2805/howto-recalibrate-variant-quality-scores-run-vqsr), we set this parameter to 99.9 which provides the highest sensitivity you can get while still being acceptably specific (this parameter may be changed in the [config.yaml](config.yaml) file).

 

Note that you need to have enough variants in your data sets (WGS or > 30 exomes). Otherwise, you will get an error “NO DATA FOUND”. If so, GATK recommends to filter the variants using VariantFiltration tool (see [Commonly called variants hard filtering](#commonly-called-variants-hard-filtering)) instead of VQSR.

 

### Genotype Filtering

The final step, is to replace ambiguous genotype called, i.e genotype called by taking into account to little reads or to many:

- min = 4
- max = average sample depth of coverage * 2.5

This is done by an home made script (see script folder), developed by Martijn Derks at the Wageningen University.

## 3. Dependencies

snakemake and java8 need to be available in your PATH.

Here is the list of the tools dependacies with the version tested and used for the IMAGE project.

Absolut paths need to be precise in the [config.yaml](config.yaml)

- bgzip v 0.2.5
- tabix v 0.2.5
- gatk v 3.7
- bcftools 1.8
- md5sum v 8.22

For the final step, the python script filter_multi_sample_vcf.py need PyVCF for Python3

## 4. Configure and running workflow

- Copy the config.yaml file into your working directory and update it according to the instructions inside the file.
- If necessary update resources_SLURM.yaml to define cpu memory and cluster partition resources for each rule. The default section is the minimal resources per job. The other section take as name the rules name and define only resources that are different from the default section.
- launch snakemake command as presented in SLURM.sh to launch the workflow.

Resources_SLURM.yaml and SLURM.sh are specific to SLURM cluster. Therefore, if you use another cluster, you need to create other files. However, you can use these files to inspire you.
​    

This is the official documentation of Snakemake if you need more information: <https://snakemake.readthedocs.io/en/stable/tutorial/tutorial.html>

 
